package ru.alfabank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Solution {

    public static void main(String[] args) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get("1.txt"));
        String line = lines.get(0);
        String[] str = line.split(",");
        Integer[] array = new Integer[str.length];

        for (int i = 0; i < str.length; i++) {
            array[i] = Integer.parseInt(str[i]);
        }

        Arrays.sort(array);

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        System.out.println();

        Arrays.sort(array, Collections.reverseOrder());

        for (int i =  0; i < array.length ; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
